<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Registro extends Main {
        
	public function __construct()
	{
            parent::__construct();
            if(!empty($_SESSION['user']))
                header("Location:".base_url('panel'));            
	}
        
        public function index($url = 'main',$page = 0)
	{
            $crud = new ajax_grocery_CRUD();
            $crud->set_theme('flexigrid');
            $crud->set_table('user');
            $crud->set_subject('Usuarios');               
            //Fields            
            //unsets
            $crud->unset_delete()                                  
                 ->unset_fields()
                 ->unset_back_to_list()
                 ->unset_edit()
                 ->unset_list()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_export()
                 ->unset_print()
                 ->field_type('cuenta','invisible')
                 ->field_type('status','invisible')
                 ->field_type('password','password')
                 ->unset_columns('password');
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('panel').'"</script>');
            $crud->set_lang_string('form_save','Registrarse');
            //Displays
            $crud->display_as('password','Contraseña');            
            //Fields types            
            //Validations  
            $crud->required_fields('nombre','email','password');              
            $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');            
            $crud->set_rules('password','Contraseña','required|min_length[8]');                                            
            //Callbacks            
            $crud->callback_before_insert(array($this,'binsertion'));
            $crud->callback_after_insert(array($this,'ainsertion'));            
            $output = $crud->render();
            $output->view = 'main';
            $output->crud = 'user';
            $this->loadView($output);
	}
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($post,$id)
        {    
            $this->user->login_short($id);            
            return true;
        }
        
        function forget($key = '')
        {
            if(empty($_POST) && empty($key))
            $this->loadView('forget');
            else
            {
                if(empty($key)){
                if(empty($_SESSION['key'])){
                $this->form_validation->set_rules('email','Email','required|valid_email');                
                if($this->form_validation->run())
                {      
                    
                    $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                    if($user->num_rows>0){                                                
                        correo($this->input->post('email'),utf8_decode('Recuperación de Contraseña'),$this->load->view('email/forget',array('user'=>$user->row()),TRUE));
                        $_SESSION['msj'] = $this->success('Los pasos para la restauracion han sido enviados a su correo electronico');
                        header("Location:".base_url('registro/forget'));
                        //$this->loadView(array('view'=>'forget','msj'=>$this->success('Los pasos para la restauracion han sido enviados a su correo electronico')));
                    }
                    else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error('Los datos suministrados son incorrectos.')));
                }
                else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error($this->form_validation->error_string())));
                }
                else
                {
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    $this->form_validation->set_rules('pass','Password','required|min_length[8]');
                    $this->form_validation->set_rules('pass2','Password2','required|min_length[8]|matches[pass]');
                    $this->form_validation->set_rules('key','Llave','required');
                    if($this->form_validation->run())
                    {
                        if($this->input->post('key') == $_SESSION['key'])
                        {
                            $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$_SESSION['email']));
                            session_unset();
                            $this->loadView(array('view'=>'forget','msj'=>$this->success('Se ha restablecido su contraseña')));
                        }
                        else
                            $this->loadView(array('view'=>'recover','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                    }
                    else{
                        if(empty($_POST['key'])){
                        $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));    
                        session_unset();
                        }
                        else
                        $this->loadView(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
                    }
                }
                }
                else
                {
                    if(!empty($_SESSION['key']) && $key==$_SESSION['key'])
                    {
                        $this->loadView(array('view'=>'recover','key'=>$key));
                    }
                    else
                    $this->loadView(array('view'=>'forget','msj'=>$this->error('Se ha vencido el plazo para el restablecimiento, solicitelo nuevamente.')));
                }
            }
        }
        
        function direccionField()
        {
            return form_input('direccion','','id="field-direccion" placeholder="Calle/Avenida/Residencia/Casa"');
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */