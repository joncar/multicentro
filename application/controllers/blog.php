<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Blog extends Main {
        
	public function __construct()
	{
		parent::__construct();
	}
        
        public function index($url = '',$id = '')
	{
            if(empty($url)){
                $this->db->order_by('id','DESC');
		$this->loadView(array('view'=>'blog','entrys'=>$this->db->get('blog')));
            }
            elseif($url=='cat')
                $this->cat($id);
            else
                $this->blogview($url);
	}              
        
        function cat($id){
            $this->db->select('blog.*, categorias.nombre as cat');
            $this->db->join('categorias','categorias.id = blog.categoria','inner');
            $this->db->order_by('id','DESC');
            $cat = $this->db->get_where('blog',array('categoria'=>$id));
            if($cat->num_rows>0){
                $bread = array(
                    array('cat/'.$id,$cat->row()->cat)
                );
                $this->loadView(array('view'=>'blog','title'=>$cat->row()->cat,'entrys'=>$cat,'bread'=>$bread));
            }
            else $this->loadView('404');
        }
        
        function blogview($url){
            $titulo = urldecode(str_replace("-","+",$url));
            $this->db->select('blog.*, categorias.nombre as cat');
            $this->db->join('categorias','categorias.id = blog.categoria','inner');
            $entry = $this->db->get_where('blog',array('titulo'=>$titulo));
            if($entry->num_rows>0){
                $entry = $entry->row();
                $bread = array(
                    array('cat/'.$entry->categoria,$entry->cat),
                    array(str_replace('+','-',urlencode($entry->titulo)),$entry->titulo)
                );
                $title = ucwords(str_replace("_"," ",$entry->titulo));
                $this->loadView(array('view'=>'main','entry'=>$entry,'title'=>$title,'bread'=>$bread,'thumbnail'=>base_url('img/'.$entry->imagen)));
            }
            else $this->loadView('404');
        }
        
        function portafolio($url){
            $portafolio = $this->db->get_where('portafolio',array('nombre'=>str_replace("-"," ",$url)));
            if($portafolio->num_rows>0){
                $this->loadView(array('view'=>'portafolioview','title'=>$url,'entry'=>$portafolio->row(),'fotos'=>$this->db->get_where('portafolio_fotos',array('portafolio'=>$portafolio->row()->id))));
            }
            else $this->loadView('404');
        }
        /*Cruds*/       
}   
    

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */