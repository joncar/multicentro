<?php

class Prodmodel extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }
    
    function getCats()
    {
        $x = $this->db->get('categorias_productos');
        foreach($x->result() as $d=>$c)
        {
            $x->row($d)->entry = $this->db->get_where('productos',array('categoria'=>$c->id))->num_rows;
        }
        return $x;
    }
    
    function getEntrys()
    {
        $this->db->select('productos.*,categorias_productos.nombre as catnombre');
        $this->db->join('categorias_productos','categorias_productos.id = productos.categoria');
        return $this->db->get('productos');
    }
    
    function getFotos($id)
    {
        return $this->db->get_where('fotos',array('producto'=>$id));
    }
}
?>
