<?php
class Querys extends CI_Model{
    protected  $banners;
    
    function __construct(){
            parent::__construct();
            $this->banners = array();
    }
    
    function get_conf()
    {
        $conf = $this->db->get_where('ajustes')->row();
        $conf->title = $conf->titulo_pagina;
        return $conf;
    }
    
    function get_page($url)
    {
        return $this->db->get_where('paginas',array('url'=>$url))->row();
    }
    
    function get_table($table){
        return $this->db->get_where($table)->result();
    }
    
    function get_publicidad($posicion){
        foreach ($this->banners as $y){
            $this->db->where('id != ',$y);
        }
        $publicidad = $this->db->get_where('publicidad',array('posicion'=>$posicion));
        if($publicidad->num_rows>0){
            $max = $publicidad->num_rows;
            $ban = $publicidad->row(rand(0,$max-1));
            $this->banners[] = $ban->id;
            return $ban;
        }
        else return 0;
    }
}
