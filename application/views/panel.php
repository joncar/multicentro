<?= $this->load->view('includes/header'); ?>
<div class="container">
<? if(empty($_SESSION['user'])): ?>
    <div class='col-xs-12 col-sm-5 col-sm-offset-4'><? $this->load->view('predesign/login'); ?></div>
<? else: ?>    
    <div class='row'>
        <div class='col-xs-12 col-sm-2' style="background:lightgray; padding-top:20px; padding-bottom:20px;">
            <ul class="nav nav-pills nav-stacked" role="tablist">
                <li>Panel de control</li>
                <?php if($_SESSION['cuenta']==1): ?>
                <li><a href="<?= base_url('panel/user') ?>"><i class="fa fa-user"></i> Usuarios</a></li>                
                <li><a href="<?= base_url('panel/ajustes') ?>"><i class="fa fa-wrench"></i> Ajustes</a></li>
                <li><a href="<?= base_url('panel/categorias') ?>"><i class="fa fa-archive"></i> Categorias</a></li>
                <li><a href="<?= base_url('panel/blog') ?>"><i class="fa fa-archive"></i> Paginas</a></li>
                <li><a href="<?= base_url('panel/images') ?>"><i class="fa fa-image"></i> Banner</a></li>                
                <li><a href="<?= base_url('panel/publicidad') ?>"><i class="fa fa-image"></i> Publicidad</a></li>
                <?php endif ?>                
                <li><a href="<?= base_url('main/unlog') ?>"><i class="glyphicon glyphicon-remove"></i> Salir</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-10 well">
           <?php if(!empty($crud)): ?>
                <? $this->load->view('cruds/'.$crud); ?>
           <?php else: ?>
            <h1>Panel de control</h1>
            <p><b>Cantidad de visitas: </b> <?= $this->db->get('visitas')->num_rows ?></p>
           <?php endif ?>
        </div>
    </div>    
<? endif; ?>
</div>
