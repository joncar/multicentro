<ul class="menu">
    <li><a href="<?= site_url() ?>">Inicio <div></div></a></li>
    <li><a href="<?= site_url('quienes_somos') ?>">Quienes somos <div style="background:#eb008b"></div></a></li>
    <li><a href="<?= site_url('directorio') ?>">Directorio <div style="background:#283c64"></div></a></li>
    <li><a href="<?= site_url('servicios') ?>">Servicios <div style="background:#f79b3b"></div></a></li>
    <li>
        Alquiler <div style="background:#86d2e2"></div>
        <ul style="background:#86d2e2; border:1px solid white;">
            <li><a href="<?= site_url('salon-de-conferencias') ?>">Salón de conferencias</a></li>
            <li><a href="<?= site_url('sala-de-fiestas') ?>">Sala de fiestas</a></li>
            <li><a href="<?= site_url('espacios-publicitarios') ?>">Espacios publicitarios</a></li>
        </ul>
    </li>
    <li><a href="<?= site_url('proyecto') ?>">Proyecto <div style="background:#eb008b"></div></a></li>
    <li><a href="<?= site_url('multimedia') ?>">Boletin <div style="background:#add045"></div></a></li>
    <li>
        Condominio Mede <div style="background:#0e85c7"></div>
        <ul style="background:#0e85c7; border:1px solid white;">
            <li><a href="<?= site_url('condominio') ?>">Condominio</a></li>
            <li><a href="<?= site_url('normativas') ?>">Normativas</a></li>
        </ul>
    </li>
</ul>
