<footer class="row" style="border-top:2px solid #cccccc; padding:40px 0 20px 0; margin-top:30px;">
    <div class="col-xs-8">
        <a href="<?= site_url() ?>">INICIO </a> |
        <a href="<?= site_url('nosotros') ?>">NOSOTROS </a> |
        <a href="<?= site_url('productos') ?>">PRODUCTOS </a> |
        <a href="<?= site_url('clientes') ?>">CLIENTES </a> |
        <a href="<?= site_url('contacto') ?>">CONTACTO </a> 
    </div>
    <div class='col-xs-4'>
        Derechos reservados © Copyright <?= date("Y") ?>
    </div>
</footer>