<header class="container">
    <!--- Contacto -->
    <div class="row" style='margin-bottom:10px;'>                
        <div class="col-xs-2">
            <a class="" href="<?= site_url() ?>"><?= img('img/'.$this->db->get('ajustes')->row()->foto,'width:80%;') ?></a>

        </div>
        <div class="col-xs-10 col-sm-10" align="right" style="font-size:22px;  padding: 20px 0;">
            <a href="<?= $this->db->get('ajustes')->row()->facebook ?>"><i class="fa fa-facebook-square"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->twitter ?>"><i class="fa fa-twitter-square"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->instagram ?>"><i class="fa fa-instagram"></i></a>
            <a href="<?= $this->db->get('ajustes')->row()->linkedin ?>"><i class="fa fa-linkedin"></i></a>
        </div>
    </div>
    <div class="row">
        <?php $data = array(); foreach($this->db->get('banner')->result() as $b){ $data[] = 'img/'.$b->foto;} ?>
        <?php $this->load->view('predesign/carousel',array('data'=>$data)) ?>
    </div>
    <div class="row" style="padding-top:10px;">
        <?php $this->load->view('includes/nav'); ?>
    </div>
</header>
