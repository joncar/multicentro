<div class="blog">
<? $this->load->view('includes/blog_header'); ?>
<section class="container" style="padding:40px 0;">
    <div class="row">
        <ul class="list-group">
            <?php foreach($entrys->result() as $b): ?>
                <li class="list-group-item row">
                    <div class="col-xs-3" style="background:url(<?= base_url('img/'.$b->imagen) ?>) no-repeat; background-position:top; height:200px;">
                        
                    </div>
                    <div class="col-xs-9">
                        <h3><?= $b->titulo ?></h3>
                        <p><?= strip_tags(substr($b->texto,0,100)).'...' ?></p>
                        <p><a href="<?= site_url('blog/'.str_replace('+','-',urlencode($b->titulo))); ?>" class="btn btn-primary" role="button">Ver más</a></p>
                    </div>                                    
                </li>        
            <?php endforeach ?>
        </ul>        
</section>
<script>
    $(document).ready(function(){        
       $("header nav").css('background','#333');
    })
</script>
</div>